# Homework:
# Write a program that reads the contents of the file into a list. 
# The program should then ask the user to enter a charge account number (a seven digit number, no more no less). The program should determine whether the number is valid by searching for it in the list. 
# If the number is in the list, the program should display a message indicating the number is valid. 
# If the number is not in the list, the program should display a message indicating the number is invalid.

def read_file():
    # Open a file name charge_accounts.txt.
    infile = open('charge_accounts.txt','r')
    # Create list 
    list_read_line = []
    for line in infile:
        # Read three lines from the file.
        list_read_line.append(line)

    # Close the file.
    infile.close()
    
    # Show the content of list
    for line in range(len(list_read_line)):
        print ('Linea {}:  {}' .format(line, list_read_line[line]))
    
   
    
#main function.
print ("\n Welcome this program will search your account number in the file. \n")
user_account = input("Please, enter a account number with seven digit number: ")


print(user_account.isdigit())

    
if len(user_account)== 7:
    valid_number=int(user_account)
    print (user_account)
    print('Cantidad de digitos ', len(user_account))
else:
    print("You must enter a number with nine digit. Try again.")



#read_file()

